GRU Devlog 11 - yemu and channel on libera.chat

I am making this devlog a bit earlier, because I will be busy on weekends.

This week I didn't have plan, but it seems I have done some things to yemu: added loading programs 
from binary file and several instructions - TAX, TAY and NOP.

I think gasm will be made for 6502 firstly, because it is easier then x86 and it would help in 
testing yemu.

I was trying to make some daily notes in Org Roam (and moved devlog notes there), but it wasn't 
very success, but I hope to do it more often :)

Also I have registered #gru channel on libera, so you can join us also on libera.chat :)

Hope you liked this post :). If you would like to help me, contact me via email, xmpp or irc :)

tags: gru, yemu, libera, org-mode, org-roam
