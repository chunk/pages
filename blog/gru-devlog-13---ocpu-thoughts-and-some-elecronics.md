GRU Devlog 13 - ocpu thoughts and some elecronics

This week was very busy week at school and I had no time for programming.

But in the start of week I was thinking about making ocpu specs more Turing-complete.
I still think it is not done but maybe it will in some time (and of course more description required).

But I had time to buy some transistors and other electronics. Currently I am trying to make half-adder.

I have created tiny log page - [lolcpu](https://g1n.ttm.sh/lolcpu/). I am going to post there some of my
electronic ideas, circuits and progress in doing some electronic things. 

Hope you liked this post! If you would like to help me, contact me via email, xmpp or irc :)

tags: gru, lolcpu, ocpu, transistors, electonics
