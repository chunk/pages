GRU Devlog 12 - yemu, gasm and ocpu

This week I have done several new instructions for yemu. For example all transfer instructions.

Also I have rewritten gasm to support 6502, published it and it should help me with debugging yemu.
It supports not very a lot instructions but I hope it is good start.

But I think the coolest what I did this week is ocpu proccessor specification.
Currently it is draft but i have already published it [here](https://g1n.ttm.sh/gru/ocpu).
I hope this specifications will be Turing complete and we will try to implement it in real world.
But firstly we need to make this specififcations complete, assembler and emulator for it.

Also smlckz's idea was to make translator from ocpu assembler instructions to avr to emulate it on arduino!

Hope you liked this post! If you would like to help me, contact me via email, xmpp or irc :)

tags: gru, yemu, gasm, ocpu
